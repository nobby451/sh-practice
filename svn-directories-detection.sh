#!/bin/sh

svn log -v $1 | grep -Po '(?<=^   A /)[^/]*(?=\s|$)' | sort | uniq
