#!/bin/sh

if [ `id -u` -gt 0 ]; then
  echo このスクリプトはroot権限で実行して下さい
else
  sleep 30m
  pm-suspend
fi
