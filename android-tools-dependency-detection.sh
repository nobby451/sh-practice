#!/bin/sh

find *tools/ -type f -print0 | xargs -0 file | grep 32-bit | awk -F: '{ print $1 }' | xargs ldd
